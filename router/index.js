const express = require("express");
const router = express.Router();
const bodyParse= require("body-parser");


router.get('/luz',(req,res)=>{

    const valores={
        boleto:req.query.boleto,
        nombre:req.query.nombre,
        domicilio:req.query.domicilio,
        tipo:req.query.tipo,
        consumo:req.query.consumo,
        descontado:req.query.descuentado,
        descuento:req.query.descuento,
        total:req.query.total,
        subtotal:req.query.subtotal
    }
   
    
    res.render('luz.html',valores)


})

router.post("/luz",(req,res)=>{
    const valores={
        boleto:req.body.boleto,
        nombre:req.body.nombre,
        domicilio:req.body.domicilio,
        tipo:req.body.tipo,
        consumo:req.body.consumo,
        descontado:req.body.descuentado,
        descuento:req.body.descuento,
        total:req.body.total,
        subtotal:req.body.subtotal
    }
   
    res.render('recibo.html',valores);

})
module.exports=router;